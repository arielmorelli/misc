local vim = vim -- to avoid undefined vim warning all down the file

local Plug = vim.fn['plug#']


vim.call('plug#begin', '~/.config/nvim/plugged')

Plug 'danilo-augusto/vim-afterglow'

Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'

Plug 'sheerun/vim-polyglot'

Plug('neoclide/coc.nvim', {branch = 'release'})

Plug 'preservim/nerdtree'

Plug 'github/copilot.vim'

Plug('CopilotC-Nvim/CopilotChat.nvim', {branch = 'main' })

vim.call('plug#end')

-- colorscheme
vim.cmd('colorscheme afterglow')

-- telescope
local builtin = require('telescope.builtin')
vim.keymap.set('n', '<C-p>', builtin.find_files, {})
vim.keymap.set('n', '<C-l>', builtin.live_grep, {})

-- -- coc
-- function _G.check_back_space()
--     local col = vim.fn.col('.') - 1
--     return col == 0 or vim.fn.getline('.'):sub(col, col):match('%s') ~= nil
-- end


local keyset = vim.keymap.set
local opts = {silent = true, noremap = true, expr = true, replace_keycodes = false}
keyset("i", "<TAB>", 'coc#pum#visible() ? coc#pum#next(1) : v:lua.check_back_space() ? "<TAB>" : coc#refresh()', opts)
keyset("i", "<S-TAB>", [[coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"]], opts)
keyset("i", "<cr>", [[coc#pum#visible() ? coc#pum#confirm() : "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"]], opts)
keyset("i", "<c-space>", "coc#refresh()", {silent = true, expr = true})
keyset("n", "gd", "<Plug>(coc-definition)", {silent = true})
keyset("n", "gy", "<Plug>(coc-type-definition)", {silent = true})
keyset("n", "gi", "<Plug>(coc-implementation)", {silent = true})
keyset("n", "gr", "<Plug>(coc-references)", {silent = true})

-- nerdtree
keyset({"n", "i", "v", "c"}, "<F2>", "<cmd>NERDTreeToggle %<CR>", {expr = false})

keyset({"n", "i", "v", "c"}, "<F9>", "<cmd>Copilot<CR>", {expr = false})



-- ruff config
-- require('lspconfig').ruff_lsp.setup {
--     init_options = {
--       settings = {
--         args = {},
--       }
--     }
--   }


-- Copilot
-- disable because of subscription
vim.g.copilot_enabled = false

require("CopilotChat").setup {
}

