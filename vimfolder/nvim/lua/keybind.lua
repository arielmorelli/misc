local vim = vim

local opts = { noremap = true, silent = true }

function toggle_numbers()
    vim.fn.execute("set number! relativenumber!")
end

vim.keymap.set('n', '°', '<cmd>lua toggle_numbers()<CR>', opts)

-- vim.api.nvim_create_user_command('Fix', ':CocCommand ruff.executeAutofix', {bang = true})

