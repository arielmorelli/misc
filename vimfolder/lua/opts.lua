local vim = vim -- gets rid of warnings for every global undefined vim variable

local keyset = vim.keymap.set

vim.opt.guicursor = ""
vim.opt.mouse = ""

vim.opt.backup = false
vim.opt.swapfile = false

vim.opt.relativenumber = true
vim.opt.number = true
vim.opt.cursorline = true

vim.opt.wrap = true
vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.expandtab = true
vim.opt.shiftwidth = 4
vim.opt.smartindent = true

vim.opt.showmatch = false
vim.opt.hlsearch = true

vim.opt.hidden = true

vim.opt.ignorecase = false
vim.opt.smartcase = true
vim.opt.undofile = false

vim.opt.scrolloff = 2
vim.opt.cmdheight = 1 -- message size
vim.opt.equalalways = false -- New vim windows created won't make everything back to same sizes

vim.opt.autoread = true -- autoreads file if changed by other editor

-- disabled until find how not to include in the pum
-- vim.opt.spell = true
-- vim.opt.spelllang = "en_us" -- use z= to get word suggestions from vim. zg to add a word to dictionary and zw will mark word as wrong

vim.opt.termguicolors = true
vim.opt.colorcolumn = "140"

-- Don't pass messages to |ins-completion-menu|.
vim.opt.shortmess:append("c")
vim.opt.shortmess:remove("F")

-- configure completion
vim.opt.completeopt = "menu,menuone,noselect"
vim.opt.complete:append("k")

if vim.fn.executable('rg') then
  vim.g.rg_derive_root = 'true'
end
vim.g.loaded_matchparen = 1
vim.g.mapleader = " "
vim.g.maplocalleader = "\\"
vim.g.netrw_browse_split = 2
vim.g.netrw_liststyle = 0
vim.g.vrfr_rg = 'true'
vim.g.netrw_banner = 0
vim.g.netrw_winsize = 25
-- in millisecond, used for both CursorHold and CursorHoldI,
-- use updatetime instead if not defined
vim.g.cursorhold_updatetime = 100

vim.diagnostic.config({
  float = {
    source = 'always',
  },
})

-- resize windows
local wr_group = vim.api.nvim_create_augroup('WinResize', { clear = false })

vim.api.nvim_create_autocmd(
    'VimResized',
    {
        group = wr_group,
        pattern = '*',
        command = 'wincmd =',
        desc = 'Automatically resize windows when the host window size changes.'
    }
)


-- search
function search_current_word()
    local cursor_word = vim.fn.expand("<cword>")
    vim.fn.execute("let @/ = '" .. cursor_word .. "'")
    vim.o.hlsearch = true
end

function toggle_highlight_search()
    vim.o.hlsearch = not vim.o.hlsearch
end

local opts = { noremap = true, silent = true }
keyset('n', '#', '<cmd>lua search_current_word()<CR>', opts)
keyset('n', 'ä', '<cmd>lua toggle_highlight_search()<CR>', opts)

-- popup menu
keyset('i', '<Tab>', function()
  if vim.fn.pumvisible() == 1 then return '<C-n>' end
  return '<Tab>'
end, { expr = true })

keyset('i', '<CR>', function()
  if vim.fn.pumvisible() == 1 then return '<C-y>' end
  return '<CR>'
end, { expr = true })
