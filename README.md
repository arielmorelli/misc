# Configurations

This repo contains my default config for my most used tools.

# Usage

* To install and/or update, use `bash install.sh`, it will automatically install and update each tool.

* To update this files, use `bash update_repo.sh`, it will get all files and add to the right folders here.
