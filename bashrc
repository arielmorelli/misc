# ~/.bashrc

# Use vim editor on bash
export EDITOR=vim
set -o vi
bind -x '"\C-l": clear;' # crtl+l to clear terminal

# Don't do anything if not running in interactive mode
[[ $- != *i* ]] && return

# Set location and language
export LC_ALL='en_US.UTF-8'
export LANG='en_US.UTF-8'


# Setup for bash_history
HISTCONTROL=ignoreboth
HISTSIZE=5000
HISTFILESIZE=20000
shopt -s histappend
# See bash(1) for more options

# Shell options
# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
shopt -s globstar

#  If  set,  shell error messages are written in the standard GNU error message fo
# mat.
shopt -s gnu_errfmt

# Enable bash_completion
[ -r /usr/share/bash-completion/bash_completion ] && . /usr/share/bash-completion/bash_completion


# Set colorful PS1 only on colorful terminals.
# dircolors --print-database uses its own built-in database
# instead of using /etc/DIR_COLORS.  Try to use the external file
# first to take advantage of user additions. Use internal bash
# globbing instead of external grep binary.
use_color=true
safe_term=${TERM//[^[:alnum:]]/?}   # sanitize TERM
match_lhs=""
[[ -f ~/.dir_colors   ]] && match_lhs="${match_lhs}$(<~/.dir_colors)"
[[ -f /etc/DIR_COLORS ]] && match_lhs="${match_lhs}$(</etc/DIR_COLORS)"
[[ -z ${match_lhs}    ]] \
	&& type -P dircolors >/dev/null \
	&& match_lhs=$(dircolors --print-database)
[[ $'\n'${match_lhs} == *$'\n'"TERM "${safe_term}* ]] && use_color=true

current_git_branch_for_ps1() {
	BRANCH=$(git rev-parse --abbrev-ref HEAD 2>/dev/null)
	if [ -z "$BRANCH" ] ; then
		FINAL=""
	else
		INIT_B='('
		END_B=')'
		FINAL="$INIT_B$BRANCH$END_B"
	fi
	echo "$FINAL"
}

if ${use_color} ; then
	# Enable colors for ls, etc.  Prefer ~/.dir_colors #64489
	if type -P dircolors >/dev/null ; then
		if [[ -f ~/.dir_colors ]] ; then
			eval $(dircolors -b ~/.dir_colors)
		elif [[ -f /etc/DIR_COLORS ]] ; then
			eval $(dircolors -b /etc/DIR_COLORS)
		fi
	fi

	if [[ ${EUID} == 0 ]] ; then
		PS1='\[\033[01;31m\][\h\[\033[01;36m\] \W\[\033[01;31m\]]\$\[\033[00m\] '
	else
		PS1='\[\e[0;31m\]\W\[\e[0m\]\[\e[0;33m\]$(current_git_branch_for_ps1)\[\e[m\]\$ '
	fi

	export LS_OPTIONS='--color=auto'

	eval "$(dircolors -b)"
		alias ls='ls --color=auto'
		alias grep='grep --colour=auto'
		alias egrep='egrep --colour=auto'
		alias fgrep='fgrep --colour=auto'
else
	if [[ ${EUID} == 0 ]] ; then
		# show root@ when we don't have colors
		PS1='\u@\h \W \$ '
	else
		PS1='\u@\h \w \$ '
	fi
fi

unset use_color safe_term match_lhs sh

xhost +local:root > /dev/null 2>&1

complete -cf sudo

export PATH=$PATH:/home/ariel/.local/bin
export PATH=$PATH:~/.yarn/bin

# Alias
alias timestamp="date +%Y%m%d%H%M%S"

# Yaort
export YAOURT_COLORS="nb=1:pkg=1:ver=1;32:lver=1;45:installed=1;42:grp=1;34:od=1;41;5:votes=1;44:dsc=0:other=1;35"

# Vim config
_check_vim_files() {
    echo 'Checking vim files...'
    if [ ! -d ~/.vim/autoload ]; then
        echo '* Creating plugin directory'
        curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
        echo '* Installing plugins. It can take a while, if you prefer you cancel this operation and run :PlugInstall inside vim.'
        nvim +PlugInstall +qall 2>&1
    	echo "Done"
    fi
}

# alias vim='_check_vim_files; nvim'
alias vim='nvim'

# Clipboard
alias to_clip="xclip -sel clip"
alias clip="xclip -sel clip -o"

# Todo
alias todolist="touch ~/.todolist; cat ~/.todolist"
alias todo="nvim ~/.todolist"

# Coding
codestyle() {
	if [[ -z "$@" ]]; then
		folders=$(find . -maxdepth 1 -type d -not -name "." -not -name ".git" -not -name "env" -not -name "venv" -not -name ".venv" -not -name ".env" -not -regex ".*pycache.*" -printf '%f\n')
	else
		folders=${@}
	fi

	echo -e "Checking folders:\n$folders\n"
	echo -e "-----------------\nWith pycodestyle:"
	pycodestyle $folders
	echo -e "-----------------\nWith flake8:"
	flake8 $folders
	echo -e "-----------------\nWith black:"
	black --diff $folders
	echo -e "-----------------\nWith isort:"
	isort --diff $folders
}

alias docker-clean-images='docker rmi $(docker images -f "dangling=true" -q)'
alias docker-reset="sudo systemctl restart docker"
alias docker-compose="docker compose --compatibility $@"
alias docker-prune-all="docker container prune -f && docker image prune -a && docker system prune -af"

# Python config
alias python="python3"
penv-clean() {
        find . -type f -name "*.py[co]" -delete
        find . -type d -name "__pycache__" -delete
}
alias nosetestscoverage="nosetests --with-coverage --cover-package=src "
alias penv-activate="source .venv/bin/activate"

alias penv-recreate="rm -rf .venv; python3.11 -m venv .venv; penv-activate; pip install -U pip"
alias penv-recreate13="rm -rf .venv; python3.13 -m venv .venv; penv-activate; pip install -U pip"

penv-install-all() {
	files_to_install=""
	if [[ -f "requirements.txt" ]]; then
		files_to_install+=" -r requirements.txt"
	fi
	if [[ -f "dev-requirements.txt" ]]; then
		files_to_install+=" -r dev-requirements.txt"
	fi
	if [[ -f "requirements-dev.txt" ]]; then
		files_to_install+=" -r requirements-dev.txt"
	fi
	if [[ -f "requirements-integration.txt" ]]; then
		files_to_install+=" -r requirements-integration.txt"
	fi
	if [[ -f "requirements-contract.txt" ]]; then
		files_to_install+=" -r requirements-contract.txt"
	fi

	echo $files_to_install
	penv-activate && pip install $files_to_install
}
alias penv-recreate-install="penv-recreate && penv-install-all"
alias penv-recreate-install13="penv-recreate13 && penv-install-all"

# Git alias
git_reset_hard() {
 	git reset --hard origin/$(git rev-parse --abbrev-ref HEAD)
}
git_push_origin() {
 	git push --set-upstream origin $(git rev-parse --abbrev-ref HEAD)
}

nsk8s() {
	target_ns=$1
	kubectl config set-context --current --namespace="$target_ns"
}

# Go config
export GOPATH=~/projects
export PATH=$PATH:/usr/local/go/bin:$GOPATH/bin


complete -C /usr/bin/terraform terraform
source <(kubectl completion bash)
source <(local-deployment-cli-k8s completion)
. "$HOME/.cargo/env"
