# Bash
cp ~/.bashrc bashrc 

# Vim
cp ~/.vimrc vimrc
cp -r ~/.config/nvim vimfolder

# Terminator
cp ~/.config/terminator/config terminator.config 

# I3
cp -r ~/.config/i3/ i3/*  
