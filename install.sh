echo "Installing/Updating .bashrc (~/.bashrc)"
cp bashrc ~/.bashrc

echo "Installing/Updating .vimrc (~/.vimrc)"
mkdir -p ~/.config/nvim
cp -r vimfolder ~/.config/nvim
echo "  installing required python libraries"
pip install -U jedi-language-server mypy black isort flake8 pycln

echo "Installing/Updating i3 (~/.config/i3/*)"
mkdir -p ~/.config/i3
cp -r i3/* ~/.config/i3/

echo "Installing/Updating Terminator (~/.config/terminator/config)"
if ! command -v terminator &> /dev/null
then
	echo "Installing Terminator"
	sudo apt update && sudo apt install terminator -y
fi
mkdir -p ~/.config/terminator
cp terminator.config ~/.config/terminator/config

