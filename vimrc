" Global config
set nobackup " don't create backup file
set noswapfile " don't create swap file

" Search direction
nnoremap # *N

" Auto resize
autocmd VimResized * wincmd =
  
" Basic config
set number " Show numbers
set number relativenumber " show line distance between the current position
set tabstop=4 " tab = 4 space
set shiftwidth=4 " ident of 4 spaces
set hlsearch " highlight seach
set magic " make regex search easier
set encoding=utf8
set cursorline
" set spell

filetype indent plugin on

syntax on

colorscheme desert " Default color if other fail to load

" Remap keys
" Disable highlight
nnoremap \ :nohls<enter>
nnoremap ä :nohls<enter>
nnoremap Ä :nohls<enter>
" Use arrow keys instead of hjkl
nnoremap <A-Left> <c-w>H
nnoremap <A-Down> <c-w>J
nnoremap <A-Up> <c-w>K
nnoremap <A-Right> <c-w>L

" Configure numbers - enable " key enable/disable number and relative number
nnoremap ° :set number! relativenumber!<CR>
nnoremap " :set number! relativenumber!<CR>
:augroup numbertoggle
:  autocmd!
:  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
:  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
:augroup END

" Auto complete
inoremap <TAB> <C-R>=AutoComplete("next")<CR>
inoremap <S-TAB> <C-R>=AutoComplete("back")<CR>
function AutoComplete(arg)
    let lastChar = col(".") - 1
    if ! lastChar || getline(".")[lastChar - 1] !~ '\k'
        return "\<Tab>"
    endif
    if a:arg == "next"
        return "\<C-n>"
    else
      return "\<C-p>"
endfunction

" Makefile - use tab instead of space (will break otherwise)
if (expand("%:t") == "makefile")
    :set noexpandtab
endif

" Plugins
call plug#begin()
  " Syntax highlight
  Plug 'sheerun/vim-polyglot'

  " Colorschema
  Plug 'danilo-augusto/vim-afterglow'

  " NerdTree
  Plug 'preservim/nerdtree'

  " PYTHON

  " Jedi vim - basic python IDE
  Plug 'davidhalter/jedi-vim'

  " Linter
  Plug 'dense-analysis/ale'

  " GO
  " Go tools
  Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }

call plug#end()
 
" Colocrschema config - force colorscheme be afterglow. If fails return to vim default
colorscheme afterglow

" NERDTree
" Opens current file folder
nnoremap <F2> :NERDTreeToggle %<enter>
