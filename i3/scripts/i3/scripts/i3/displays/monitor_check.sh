DEFAULT_DISPLAY=$(xrandr | grep " primary" | cut -d" " -f 1) 
SECONDARY_WORKSPACE="9"
BRIGHTNESS="0.8"
SLEEP_TIME="0.1"

# Workspace
get_list_of_workspaces() {
    for workspace in $(i3-msg -t get_workspaces | jq -r '.[].name' | sort); do
        echo $workspace
    done
}

get_current_workspace() {
    echo $(i3-msg -t get_workspaces | jq -r '.[] | select(.focused==true).name')
}

# Display
get_connected_displays() {
    echo $(xrandr | grep " connected " | cut -d" " -f 1) | tr ' ' '\n'
}

get_second_display() {
    for display in `get_connected_displays`; do
        if [[ "$display" != "$DEFAULT_DISPLAY" ]]; then
            echo $display
            break
        fi
    done
}

get_other_display() {
    second_display=$(get_second_display)
    for display in `get_connected_displays`; do
        if [[ "$display" != "$DEFAULT_DISPLAY" && "$display" != "$second_display" ]]; then
            echo $display
        fi
    done
}

change_brightness() {
    for monitor in $(xrandr | grep " connected" | cut -f1 -d " ")
    do
        xrandr --output "$monitor" --brightness $BRIGHTNESS
    done
}

n_active_displays=$(get_connected_displays | wc -l)
current_directory=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
sh $current_directory/screens/$n_active_displays.sh  # run script for the right number of monitors
sleep $SLEEP_TIME

case $n_active_displays in
    "1")
        for workspace in $(get_list_of_workspaces); do
            i3 "workspace $workspace, move workspace to output eDP-1"
        done
        ;;
    "2")
        display2=$(get_second_display)
        for workspace in $(get_list_of_workspaces); do
            if [[ "$workspace" != "$SECONDARY_WORKSPACE" ]]; then
                i3 "workspace $workspace, move workspace to output $display2"
            fi
        done
        i3 "workspace $SECONDARY_WORKSPACE, move workspace to output $DEFAULT_DISPLAY"
        ;;
    "3")
        display2=$(get_second_display)
        display3=$(get_other_display)
        first_run="true"
        for workspace in $(get_list_of_workspaces); do
            if [[ "$workspace" != "$SECONDARY_WORKSPACE" ]]; then
                if [[ "$first_run" == "true" ]]; then
                    i3 "workspace $workspace, move workspace to output $display3"
                    first_run="false"
                    continue
                fi
                i3 "workspace $workspace, move workspace to output $display2"
            fi
        done
        echo $SECONDARY_WORKSPACE
        i3 "workspace $SECONDARY_WORKSPACE, move workspace to output $DEFAULT_DISPLAY"
        ;;
    *)
        echo "not implemented yet"
        ;;
esac

i3 workspace $current_workspace

notify-send -i display "Loaded for: $(get_connected_displays | tr '\n' ' ')" -t 1500
change_brightness

