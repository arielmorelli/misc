#!/bin/sh
xrandr --output eDP-1 --primary --mode 1366x768 --pos 5120x672 --rotate normal --output DP-1 --mode 2560x1440 --pos 2560x0 --rotate normal --output HDMI-1 --off --output DP-2 --mode 2560x1440 --pos 0x0 --rotate normal --output HDMI-2 --off --output DP-3 --off
