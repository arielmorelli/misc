# Please see http://i3wm.org/docs/userguide.html for a complete reference!
set $mod Mod1

# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below.
font pango:Terminus 14px

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# start a terminal
bindsym $mod+Return exec terminator

# kill focused window
bindsym $mod+Shift+q kill

# start dmenu (a program launcher)
bindsym $mod+d exec dmenu_run

# change focus
bindsym $mod+j focus left
bindsym $mod+k focus down
bindsym $mod+l focus up
bindsym $mod+ccedilla focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+j move left
bindsym $mod+Shift+k move down
bindsym $mod+Shift+l move up
bindsym $mod+Shift+ccedilla move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# split in horizontal orientation
bindsym $mod+h split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# focus the child container
#bindsym $mod+d focus child

# switch to workspace
bindsym $mod+1 workspace 1
bindsym $mod+2 workspace 2
bindsym $mod+3 workspace 3
bindsym $mod+4 workspace 4
bindsym $mod+5 workspace 5
bindsym $mod+6 workspace 6
bindsym $mod+7 workspace 7
bindsym $mod+8 workspace 8
bindsym $mod+9 workspace 9
bindsym $mod+0 workspace 10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace 1; workspace 1
bindsym $mod+Shift+2 move container to workspace 2; workspace 2
bindsym $mod+Shift+3 move container to workspace 3; workspace 3
bindsym $mod+Shift+4 move container to workspace 4; workspace 4
bindsym $mod+Shift+5 move container to workspace 5; workspace 5
bindsym $mod+Shift+6 move container to workspace 6; workspace 6
bindsym $mod+Shift+7 move container to workspace 7; workspace 7
bindsym $mod+Shift+8 move container to workspace 8; workspace 8
bindsym $mod+Shift+9 move container to workspace 9; workspace 9
bindsym $mod+Shift+0 move container to workspace 10; workspace 10

# reload the configuration file
bindsym $mod+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
# OLD: bindsym $mod+Shift+r restart
bindsym $mod+Shift+r exec "bash /home/ariel/.config/i3/displays/monitor_check.sh && i3-msg restart"

# exit i3 (logs you out of your X session)
bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'"

# resize window (you can also use the mouse for that)
mode "resize" {
        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym j resize shrink width 10 px or 10 ppt
        bindsym k resize grow height 10 px or 10 ppt
        bindsym l resize shrink height 10 px or 10 ppt
        bindsym ccedilla resize grow width 10 px or 10 ppt

        # same bindings, but for the arrow keys
        bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Down resize grow height 10 px or 10 ppt
        bindsym Up resize shrink height 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape
        bindsym Return mode "default"
        bindsym Escape mode "default"
}

bindsym $mod+r mode "resize"

# Start i3bar to display a workspace bar (plus the system information i3status
# finds out, if available)
bar {
        position top
        font pango:Terminus 10

        status_command i3blocks -c /home/ariel/.config/i3/i3blocks.config
}

# Lock screen with black background
bindsym $mod+Shift+x exec i3lock --color=000000
exec "xautolock -detectsleep -time 5 -noclose -locker \"i3lock -c 000000\""
exec_always --no-startup-id xautolock -time 1 -locker "i3lock --color=000000"

## printscreen
bindsym --release Print exec scrot '%Y-%m-%d_%H_%M_%s_$wx$h.png' -e 'mv $f ~/Pictures/'

## caps
bindsym --release Caps_Lock exec pkill -SIGRTMIN+10 i3blocks

## chrome
bindsym $mod+Shift+g exec /usr/bin/google-chrome --flag-switches-begin --javascript-harmony --ignore-gpu-blocklist --enable-features=ParallelDownloading --flag-switches-end

## Settings menu
bindsym $mod+F3 exec gnome-control-center

## terminal
bindsym $mod+Shift+t exec terminator

## Media player controls
bindsym XF86AudioRaiseVolume exec --no-startup-id amixer sset Master 1%+ && pkill -RTMIN+10 i3blocks
bindsym XF86AudioLowerVolume exec --no-startup-id amixer sset Master 1%- && pkill -RTMIN+10 i3blocks
bindsym XF86AudioMute exec amixer --no-startup-id sset Master toggle && pkill -RTMIN+10 i3blocks

## Set brightness
bindsym XF86MonBrightnessDown exec  --no-startup-id "brightnessctl s 10%-"
bindsym XF86MonBrightnessUp exec --no-startup-id "brightnessctl s 10%+"

## Set keymap
exec_always --no-startup-id "setxkbmap -layout de -variant deadtilde"

# Enable Scroll Lock
exec_always --no-startup-id "xmodmap -e 'add mod3 = Scroll_Lock'"

## Remove border
for_window [class="^.*"] border pixel 0

# Enable floating for zoom
# for_window [title="^zoom$"] floating enable
#

